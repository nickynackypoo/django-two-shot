# Generated by Django 5.0 on 2023-12-14 19:32

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("accounts", "0002_alter_receipt_account"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="receipt",
            name="account",
        ),
        migrations.RemoveField(
            model_name="expensecategory",
            name="owner",
        ),
        migrations.RemoveField(
            model_name="receipt",
            name="category",
        ),
        migrations.RemoveField(
            model_name="receipt",
            name="purchaser",
        ),
        migrations.DeleteModel(
            name="Account",
        ),
        migrations.DeleteModel(
            name="ExpenseCategory",
        ),
        migrations.DeleteModel(
            name="Receipt",
        ),
    ]
