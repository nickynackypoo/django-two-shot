from django.urls import path
from django.shortcuts import redirect
from .views import receipts

urlpatterns = [
    path("", receipts, name="home"),
]