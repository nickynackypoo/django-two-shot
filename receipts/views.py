from django.shortcuts import render, redirect
from .models import Receipt 
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm

# Create your views here.

@login_required
def receipts(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("home")
    else:
        receipts = Receipt.objects.all()

    context = {"receipts": receipts}
    return render(request, "receipt/list.html", context)