from django.forms import ModelForm
from receipts.models import Receipt

class RecipeForm(ModelForm):
     class Meta:
        model = Receipt
        fields = [
            "tendor",
            "total",
            "tax",
            "date",
            "category",
            "account"
            ]